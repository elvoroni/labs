package TAU_Module2;

import java.util.Scanner;

public class Lesson7_Game {
    public final static int SCISSORS = 0;
    public final static int PAPER = 1;
    public final static int STONE = 2;
    public final static int LIZARD = 3;
    public final static int SPOKE = 4;
    public final static int END = 9;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int userChoice = scanner.nextInt();

        //Далее компьтер случайно выбирает свой вариант

        int opponentValue = (int) Math.round(Math.random() * 4);

        if (userChoice == STONE) {
            if (opponentValue == STONE) {
                System.out.println("The computer chose the same thing you did! \n Tie!");
            } else if (opponentValue == PAPER) {
                System.out.println("The computer chose paper and you chose stone. \n You lose!");
            } else if (opponentValue == SCISSORS) {
                System.out.println("The computer chose scissors and you chose stone. \n You win!");
            } else if (opponentValue == LIZARD) {
                System.out.println("The computer chose lizard and you chose stone. \n You win!");
            } else {
                System.out.println("The computer chose spock and you chose stone. \n You lose!");
            }
        } else if (userChoice == PAPER) {
            if (opponentValue == STONE) {
                System.out.println("You computer chose stone, you chose paper.\n You win!");
            } else if (opponentValue == PAPER) {
                System.out.println("The computer chose the same thing you did! \n Tie!");
            } else if (opponentValue == SCISSORS) {
                System.out.println("The computer chose scissors and you chose paper. \n You lose!");
            } else if (opponentValue == LIZARD) {
                System.out.println("The computer chose lizard and you chose paper. \n You lose!");
            } else {
                System.out.println("The computer chose spock and you chose stone. \n You win!");
            }
        } else if (userChoice == SCISSORS) {
            if (opponentValue == STONE) {
                System.out.println("The computer chose stone, you chose scissors.\n You lose!");
            } else if (opponentValue == PAPER) {
                System.out.println("The computer chose paper and you chose scissors. \n You win!");
            } else if (opponentValue == SCISSORS) {
                System.out.println("The computer chose the same thing you did! \n Tie!");
            } else if (opponentValue == LIZARD) {
                System.out.println("The computer chose lizard and you chose scissors. \n You win!");
            } else {
                System.out.println("The computer chose spock and you chose scissors. \n You lose!");
            }
        } else if (userChoice == LIZARD) {
            if (opponentValue == STONE) {
                System.out.println("The computer chose stone, you chose lizard.\n You lose!");
            } else if (opponentValue == PAPER) {
                System.out.println("The computer chose paper and you chose scissors. \n You win!");
            } else if (opponentValue == SCISSORS) {
                System.out.println("The computer chose scissors you chose lizard. \n You lose!");
            } else if (opponentValue == LIZARD) {
                System.out.println("The computer chose the same thing you did! \n Tie!");
            } else {
                System.out.println("The computer chose spock and you chose lizard. \n You win!");
            }
        } else if (userChoice == SPOKE) {
            if (opponentValue == STONE) {
                System.out.println("The computer chose stone, you chose spoke.\n You win!");
            } else if (opponentValue == PAPER) {
                System.out.println("The computer chose paper and you chose spoke. \n You lose!");
            } else if (opponentValue == SCISSORS) {
                System.out.println("The computer chose scissors you chose spoke. \n You win!");
            } else if (opponentValue == LIZARD) {
                System.out.println("The computer chose lizard and you chose spoke. \n You lose!");
            } else {
                System.out.println("The computer chose the same thing you did! \n Tie!");
            }
        }
        else if (opponentValue == END){
            System.out.println("See you next time!");
        }
    }
}
