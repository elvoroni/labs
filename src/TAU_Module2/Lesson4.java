package TAU_Module2;

public class Lesson4 {
    public static void main(String[] args) {
        boolean a = true;
        boolean b = true;

        System.out.println(a & b);

        System.out.println("Логическое И:");
        System.out.println(true & true);
        System.out.println(true & false);
        System.out.println(false & true);
        System.out.println(false & false);

        System.out.println("Логическое ИЛИ:");
        System.out.println(true | true);
        System.out.println(true | false);
        System.out.println(false | true);
        System.out.println(false | false);

        System.out.println("Исключающее ИЛИ:");
        System.out.println(true ^ true);
        System.out.println(true ^ false);
        System.out.println(false ^ true);
        System.out.println(false ^ false);

        System.out.println("Логическое НЕ:");
        System.out.println(!true);
        System.out.println(!false);

        System.out.println("Логическое И-НЕ:");
        System.out.println(true & !true);
        System.out.println(true & !false);
        System.out.println(false & !true);
        System.out.println(false & !false);

        System.out.println("Логическое ИЛИ-НЕ:");
        System.out.println(true | !true);
        System.out.println(true | !false);
        System.out.println(false | !true);
        System.out.println(false | !false);


        /*int a = 10;
        int b = 20;
        int c = 30;
        int d = 40;
        boolean result = (a % 2 == 0 && b % 4 == 0) || (c % 3 == 0 && d % 3 != 0);
        System.out.println(result);*/


        int l = 91;
        System.out.println(Integer.toBinaryString(l));
    }
}
