package TAU_Module2.Lesson14;


import java.util.ArrayList;

public class BasicCat {
    public String name;
    public static Double age;
    public Double weight;

    public static Integer numKittens;

    public static final Integer koeff = 7;

    protected BasicCat(){

    }

    public BasicCat(String name) {
        this.name = name;

        if (age < 1) {
            numKittens++;

        }
    }

    public static ArrayList<BasicCat> list = new ArrayList<BasicCat>();


    public static Double getAge() {
        return age;
    }

    public void setAge(Double age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

}
