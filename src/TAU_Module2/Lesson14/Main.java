package TAU_Module2.Lesson14;


public class Main extends BasicCat {
    public static void main(String[] args) {
        BasicCat newCat = new BasicCat("Murzic");
        BasicCat newCat2 = new BasicCat("Barsic");
        BasicCat newCat3 = new BasicCat("Igor");
        BasicCat newCat4 = new BasicCat("Leo");
        BasicCat newCat5 = new BasicCat("Teo");
        BasicCat newCat6 = new BasicCat("Kit");

        newCat.setAge(2.0);
        list.add(newCat);
        newCat2.setAge(0.1);
        list.add(newCat2);
        newCat3.setAge(0.1);
        list.add(newCat3);
        newCat4.setAge(0.5);
        list.add(newCat4);
        newCat5.setAge(0.5);
        list.add(newCat5);
        newCat6.setAge(0.5);

        showAge(5.2);
        showAllKittens();

        System.out.println("Size of a list: " + list.size());

    }

    static public void showAge(Double newAge) {
        BasicCat newCat = new BasicCat("Cat");
        newCat.setAge(newAge);
        Double countAge = newCat.getAge() * koeff;
        System.out.println("This is the age of a cat: " + countAge);
    }

    static public void showAllKittens() {
        System.out.println("Amount of kittens under 1 year: " + numKittens);
    }

}