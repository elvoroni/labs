package TAU_Module2.Lesson10;

public class Main {
    public static void main(String[] args) {
        Fruit apple = new Fruit();
        Fruit banana = new Fruit();

        apple.colour = "Red";
        banana.quantity = 4;
        System.out.println("Apple colour is: " + apple.colour);
        System.out.println("Apple shape is: " + apple.shape);
        System.out.println("Quantity of bananas: " + banana.quantity);


        Music singer1 = new Music();
        Music singer2 = new Music();

        singer1.musicStyle = "Rock!!!";
        singer1.singer = "AC/DC";
        System.out.println("Singer1 informaion: " + singer1.musicStyle + "; " + singer1.singer);

        singer2.singer = "Maroon 5";
        singer2.album = 5;
        System.out.println("Singer2 information: " + singer2.singer + "; " + singer2.album);
        System.out.println("Classic music: " + Music.classic);
    }
}
