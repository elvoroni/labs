package TAU_Module2.Lesson12;

public class Square extends Shape {
    private int length;
    private static int sides;

    public Square() {
        length = 0;
        setShape("Square");
        sides = 4;
    }

    public void show(){
        System.out.println("This is " + getShape());
    }

    public void setLength(int length) {
        this.length = length;
    }

    @Override
    public void setPerimeter(double length) {
        result = sides * length;
    }

    @Override
    public void setGetAreaOfAShape(double length) {
        result = Math.pow(length, power);
    }

    public void getResult() {
        System.out.println("Result = " + result);
    }
}
