package TAU_Module2.Lesson12;

public class Main {
    public static void main(String[] args) {
        Circle circle = new Circle();
        Square square = new Square();
        Rectangle rectangle = new Rectangle();

        print(circle);

        circle.move(6, 14);

        circle.setPerimeter(5);
        circle.getResult();

        circle.setGetAreaOfAShape(5);
        circle.getResult();

        print(square);
        square.setPerimeter(2);
        square.getResult();

        square.setGetAreaOfAShape(12);
        square.getResult();

        print(rectangle);
        rectangle.setPerimeter(2, 4);
        rectangle.getResult();

        rectangle.setGetAreaOfAShape(2, 4);
        rectangle.getResult();
    }

    public static void print(Shape shape) {
        shape.show();
    }
}
