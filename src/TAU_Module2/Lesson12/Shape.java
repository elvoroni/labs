package TAU_Module2.Lesson12;

abstract public class Shape {
    protected int a;
    protected int b;
    protected double perimeter;
    protected double result;
    protected double getAreaOfAShape;
    private String shape;
    protected static int power;

    public static final double PI = 3.14;

    public Shape() {
        a = 0;
        b = 0;
        perimeter = 0;
        power = 2;
    }

    abstract public void show();

    public void setA(int a) {
        this.a = a;
    }

    public void setB(int b) {
        this.b = b;
    }

    public int getA(int a) {
        return a;
    }

    public int getB(int b) {
        return b;
    }

    public double getPerimeter() {
        return perimeter;
    }

    public void setPerimeter(double perimeter) {
        this.perimeter = perimeter;
    }

    public double getGetAreaOfAShape() {
        return getAreaOfAShape;
    }

    public void setGetAreaOfAShape(double getAreaOfAShape) {
        this.getAreaOfAShape = getAreaOfAShape;
    }

    public String getShape() {
        return shape;
    }

    public void setShape(String shape) {
        this.shape = shape;
    }

    public void move(int newA, int newB) {
        a = newA;
        b = newB;
    }
}
