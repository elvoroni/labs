package TAU_Module2;

public class Lesson3 {
    public static void main(String[] args) {
        //Целочисленные
        byte a = 126;
        short b = 32000;
        int c = 6666666;
        long d = a + b + c;
        System.out.println("byte a: " + a);
        System.out.println("short b: " + b);
        System.out.println("int c: " + c);
        System.out.println("long d: " + d);

        //С плавающей точкой
        double value;
        value = 3.654654654 * 2.01567;
        float e = 1.321f;
        System.out.println("double: " + value);
        System.out.println("float: " + e);

        //Символьная
        char symbol1 = '*';
        char symbol2 = '+';
        System.out.println("char: " + symbol1 + ", " + symbol2);

        //Логическая
        int x = 8;
        int y = 2;
        System.out.println("x > y: " + (x > y));
        System.out.println("x < y: " + (x < y));

        //String
        String firstString = "This is String :)";
        System.out.println("String: " + firstString);
    }
}
