package TAU_Module2.Lesson11;

public class Main {
    public static void main(String[] args) {
        Fruit apple = new Fruit();

        System.out.println("Previous country: " + apple.getApple().getCountry());
        System.out.println("Previous colour: " + apple.getColour());

        Apple apple1 = new Apple();
        apple1.setCountry("U.S.A");
        apple.setApple(apple1);
        System.out.println("Country is: " + apple.getApple().getCountry());

        apple.setColour("Yellow");
        System.out.println("New apple colour: " + apple.getColour());

        Fruit.quantity = 8;
        System.out.println("New quantity: " + Fruit.quantity);

        apple.buy("Ananas");
    }
}
