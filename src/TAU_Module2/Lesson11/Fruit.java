package TAU_Module2.Lesson11;

public class Fruit {
    private String colour;
    private String shape;
    public static int quantity;
    private String fruit;

    private Apple apple;

    Fruit() {
        colour = "Green";
        shape = "Round";
        quantity = 1;

        apple = new Apple();
    }

    public void buy(String newFruit){
        this.fruit = newFruit;
    }

    public String getColour(){
        return colour;
    }

    public void setColour(String newColour){
        colour = newColour;
    }

    public String getShape() {
        return shape;
    }

    public void setShape(String shape) {
        this.shape = shape;
    }

    public Apple getApple(){
        return apple;
    }

    public void setApple(Apple apple) {
        this.apple = apple;
    }
}
