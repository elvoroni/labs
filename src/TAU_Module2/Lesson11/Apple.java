package TAU_Module2.Lesson11;

public class Apple {
    private String country;

    public Apple() {
        country = "Israel";
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String newCountry) {
        country = newCountry;
    }
}
