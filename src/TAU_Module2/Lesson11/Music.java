package TAU_Module2.Lesson11;

public class Music {
    public static boolean classic = false;
    private String musicStyle;
    private String singer;
    private int album;

    Music() {
        musicStyle = "Disco";
        album = 0;
        singer = "No name";
    }

    public String getMusicStyle() {
        return musicStyle;
    }

    public void setMusicStyle(String musicStyle) {
        this.musicStyle = musicStyle;
    }

    public int getAlbum() {
        return album;
    }

    public void setAlbum(int album) {
        this.album = album;
    }

    public String getSinger() {
        return singer;
    }

    public void setSinger(String singer) {
        this.singer = singer;
    }
}

