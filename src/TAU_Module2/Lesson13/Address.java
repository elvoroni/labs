package TAU_Module2.Lesson13;

public class Address {
    public String country;
    protected String town;
    protected String street;
    protected int houseNumber;
    protected int postcode;

    public Address() {
        country = "USA";
    }

    public Address(String street) {
        this.street = street;
        houseNumber = 1;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getTown() {
        return this.town;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getStreet() {
        return this.street;
    }

    public void setHouseNumber(int houseNumber) {
        this.houseNumber = houseNumber;
    }

    public int getHouseNumber() {
        return houseNumber;
    }

    public void setPostcode(int postcode) {
        this.postcode = postcode;
    }

    public int getPostcode() {
        return postcode;
    }

    public void showInformation() {
        if (this.country.equals("USA")) {
            System.out.println("Working address: " + this.country + ", " + (town = "New York, ") + (street = "5.th Avenue, ")
                    + (houseNumber = 5) + ", " + (postcode = 1615465));
        } else if (town == null || street == null || houseNumber == 0) {
            System.out.println("Please, add full information");
        } else {
            System.out.println("Country: " + this.country);
            System.out.println("Town: " + this.town);
            System.out.println("Street: " + this.street);
            System.out.println("House number: " + this.houseNumber);
            System.out.println("Postcode: " + this.postcode);
        }
    }


    /*public void setInformation(*//*String country, String town,
                               String street, int houseNumber, int postcode*//*) {

        setTown(this.country);
        setPostcode(this.postcode);

    }*/

    /*public String getInformation(){
        return
    }*/
}
