package TAU_Module2.Lesson13;

public class Person {
    private String name;
    private int age;
    public Address country;
    private Address homeAddress;

    private Address workAddress = new Address("NEW");

    public Person() {
        name = "Tim";
        age = 55;

        country = new Address();
        homeAddress = new Address();
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    /*protected void homeAdress(){
        System.out.println("Home adress: " + country + ", "+ getTown()+ ", "+ getStreet()+
                ", "+ getHouseNumber()+ ", " + getPostcode());
    }*/

    public void workAdress() {
        if (workAddress.town == null) {
            workAddress.setTown("Berlin");
        }
        if (workAddress.street == null) {
            workAddress.setStreet("Hauptstrasse");
        }
        if (workAddress.postcode == 0) {
            workAddress.setPostcode(7867867);
        }
        System.out.println("Working adress: " + workAddress.country + ", " + workAddress.getTown() + ", " + workAddress.getStreet() + ", " +
                workAddress.houseNumber + ", " + workAddress.getPostcode());
    }


    public void homeAddress() {
        System.out.println("Home adress: " + homeAddress.country + ", " + homeAddress.getTown() + ", " + homeAddress.getStreet() +
                ", " + homeAddress.getHouseNumber() + ", " + homeAddress.getPostcode());

    }

    public void showCountry() {
        this.country = country;
    }
    /*public Address homeAddress(){
        System.out.println("Home adress: " + address.country + ", "+ address.getTown()+ ", "+ address.getStreet()+
                ", "+ address.getHouseNumber()+ ", " + address.getPostcode());

    }*/

}
