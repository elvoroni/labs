package TAU_Module2.Lesson13;

public class Main {
    private static boolean parameter = false;

    public static void main(String[] args) {

        parameter = false;
        Person person1 = new Person();
        Person person2 = new Person();
        Person person3 = new Person();


        person1.setName("Alex");
        person1.setAge(31);
        person1.country = "Germany";

        System.out.println(person1.getName() + ", " + person1.getAge());
        //person1.showInformation();

        if (parameter) {
            person1.setTown("Dresden");
            person1.setStreet("Reinstrasse");
            person1.setHouseNumber(16);
            person1.setPostcode(192029);
            person1.homeAddress();
        } else {
            person1.workAdress();
        }

        person2.setName("Arina");
        person2.setAge(23);
        person2.country = "Germany";

        System.out.println(person2.getName() + ", " + person2.getAge());
        //person2.showInformation();

        if (parameter) {
            person2.setTown("Frankfurt");
            person2.setStreet("Repperbahn");
            person2.setHouseNumber(7);
            person2.setPostcode(656565);
            person2.homeAddress();
        } else {
            person2.workAdress();
        }

        System.out.println(person3.getName() + ", " + person3.getAge() + ", " + person3.country);
        person3.showInformation();

    }
}