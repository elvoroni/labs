package TAU_Module2.Wardrobes;

public class Wardrobe {

    public String can;
    public int placeForCans;

    public Wardrobe(String can, int placeForCans){
        this.can = can;
        this.placeForCans = placeForCans;
    }

    public int getPlaceForCans() {
        return placeForCans;
    }

    public void setPlaceForCans(int placeForCans) {
        this.placeForCans = placeForCans;
    }

    public void setCan(String can) {
        this.can = can;
    }

    public String getCan() {
        return can;
    }
}
