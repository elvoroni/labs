package TAU_Module2.Lesson24;

import java.io.FileOutputStream;
import java.io.IOException;

public class Programm {

    public Programm() {
    }

    public static void main(String[] args) {
        String text = "Test";

        try {
            FileOutputStream writeInFile = new FileOutputStream("C://Users//elvoroni//Documents//test.txt");

            try {
                byte[] buffer = text.getBytes();
                writeInFile.write(buffer, 0, buffer.length);
            } catch (Throwable warning) {
                try {
                    writeInFile.close();
                } catch (Throwable var5) {
                    warning.addSuppressed(var5);
                }
                throw warning;
            }
            writeInFile.close();
        } catch (
                IOException e) {
            System.out.println(e.getMessage());
        }

        System.out.println("File has been written");
    }
}
