package TAU_Module2.Lesson24;

import java.io.FileInputStream;
import java.io.IOException;

public class Programm2 {

    public Programm2() {
    }

    public static void main(String[] args) {
        try {
            FileInputStream reader = new FileInputStream("C://Users//elvoroni//Documents//test.txt");

            try {
                System.out.printf("File size: %d bytes \n ", reader.available());

                int c;
                while ((c = reader.read()) != -1) {
                    System.out.print((char) c);
                }

            } catch (Throwable var) {
                try {
                    reader.close();
                } catch (Throwable var2) {
                    var.addSuppressed(var2);
                }

                throw var;
            }

            reader.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

    }
}
