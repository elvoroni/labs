package TAU_Module2;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Lesson8 {
    public static void main(String[] args) {
        //1
        String politeText = "Hello Alice !";
        System.out.println(politeMethod(politeText));

        //2
        makeWrapWord("hello", "(( ))");

        //3
        String s = "hard skills";
        half(s);

        //4
        endsOod("good");
        endsOod("goods");

        //5
        System.out.println(charCounter("hhuuuuurrrraaaaa", 'u'));

        //6
        double d = 12.25;
        System.out.println(oddLength(d));

        //7
        System.out.println(stringCounter("Java programmers love Java", "java"));
    }

    public static String politeMethod(String name) {
        return name;
    }

    public static void makeWrapWord(String word, String wrap) {
        String[] split = wrap.split(" ");
        System.out.println(split[0] + word + split[1]);
    }

    public static void half(String s) {
        String half = s.substring(0,4);
        System.out.println(half);
    }

    public static void endsOod(String s) {
        Boolean test = s.matches(".ood");
        System.out.println(test);
    }

    public static boolean oddLength(double d) {
        String str = Double.toString(d);
        boolean result;
        if (str.length() % 2 == 0) {
            result = false;
        } else {
            result = true;
        }
        return result;
    }

    public static int charCounter(String s, char c) {
        s = s.toLowerCase();
        int sum = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == c)
                sum++;
        }
        return sum;
    }

    public static int stringCounter(String s, String searchString){
        s = s.toLowerCase();
        Pattern p = Pattern.compile(searchString);
        Matcher m = p.matcher(s);
        int count = 0;
        while(m.find()) {
            count++;
        }
        return count;
    }
}
