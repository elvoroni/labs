package TAU_Module2.Lesson20;

public class Transport implements Typable, Soundable {
    public Transport() {
    }

    public void type() {
        System.out.println("Public Transport");
    }

    public void sound() {
        System.out.println("Ring");
    }
}
