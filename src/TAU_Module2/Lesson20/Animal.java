package TAU_Module2.Lesson20;


public class Animal implements Typable, Soundable {
    public Animal() {
    }

    public void type() {
        System.out.println("Lion");
    }

    public void sound() {
        System.out.println("Voice");
    }
}


