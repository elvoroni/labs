package TAU_Module2;

import java.util.Scanner;

public class Lesson5 {
    public static void main(String[] args) {

        //1/По номеру времени года вывести его название
        Scanner scanner = new Scanner(System.in);
        System.out.println("Number: ");
        int a = scanner.nextInt();

        if (a == 0 && a < 4) {
            System.out.println("Winter");
        } else if (a == 1) {
            System.out.println("Spring");
        } else if (a == 2) {
            System.out.println("Summer");
        } else if (a == 3) {
            System.out.println("Autumn");
        } else {
            System.out.println("Choose right number");
        }

        switch (a) {
            case 0:
                System.out.println("Winter");
                break;
            case 1:
                System.out.println("Spring");
                break;
            case 2:
                System.out.println("Summer");
            case 3:
                System.out.println("Autumn");
                break;
            default:
                System.out.println("Choose number");


                //2/Даны 3 числа, a, b, c. Проверить истинность a < b < c.
                scanner = new Scanner(System.in);
                System.out.print("Enter a: ");
                int a1 = scanner.nextInt();

                System.out.print("Enter b: ");
                int b = scanner.nextInt();

                System.out.print("Enter c: ");
                int c = scanner.nextInt();

                if (a1 >= b || b >= c || a1 >= c) {
                    System.out.println("false");
                } else {
                    System.out.println("true");
                }


                //3/Найти факториал числа
                int n = 5;
                int result = 1;
                for (int i = 1; i <= n; i++) {
                    result = result * i;
                }
                System.out.println("Factorial 5 = " + result);


                //4/Перевернуть цифры числа.

                int[] array = {1, 2, -3, 4, 5, 6, 7, 8, 9};

                for (int i = array.length - 1; i >= 0; i--) {
                    System.out.print(array[i]);
                }


                //5/Найти среднее арифметическое суммы
                int count = 1;
                int sum = 0;
                while (count <= 10) {
                    for (int i = 1; i <= 1000; i++) {

                        if (i % 3 == 0) {
                            sum += i;
                            count++;
                        }
                    }
                }
                sum = sum / 10;
                System.out.println("\n" + "Summa: " + sum);


                //6/Найти количество цифр заданного числа n типа long
                scanner = new Scanner(System.in);
                System.out.print("Enter a number: ");
                long numb1 = scanner.nextInt();
                getLenght(numb1);


                //7/Проверить, является ли число степенью двойки
                scanner = new Scanner(System.in);
                System.out.print("Input num: ");
                int num = scanner.nextInt();

                while (num != 1 && num % 2 == 0) {
                    num /= 2;
                }
                System.out.println(num == 1 ? "YES" : "NO");


                //8/генератор случайных чисел

                int f = 0;
                int l = 9;
                int l1 = 10;

                int rand1 = f + (int) (Math.random() * l);
                int rand2 = f + (int) (Math.random() * l1);

                System.out.println("Random number: " + rand1 + rand2);

                switch (rand1) {
                    case (0):
                        System.out.print("РАДИОАКТИВНЫЙ");
                    case (1):
                        System.out.print("ГРЕЧНЕВЫЙ");
                    case (2):
                        System.out.print("ДЕМОНИЧЕСКИЙ");
                    case (3):
                        System.out.print("ПРИЗРАЧНЫЙ");
                    case (4):
                        System.out.print("ОЗОРНОЙ");
                    case (5):
                        System.out.print("ПРИЗРАЧНЫЙ");
                    case (6):
                        System.out.print("КОСМИЧЕСКИЙ");
                    case (7):
                        System.out.print("ЗВЕЗДНЫЙ");
                    case (8):
                        System.out.print("СЕКСУАЛЬНЫЙ");
                    case (9):
                        System.out.print("НЕПОБЕДИМЫЙ");
                }

                switch (rand2) {
                    case (0):
                        System.out.print("КАПИТАН");
                    case (1):
                        System.out.print("ЭЛЬФ");
                    case (2):
                        System.out.print("ПИНГВИН");
                    case (3):
                        System.out.print("ИНДЕЕЦ");
                    case (4):
                        System.out.print("ГНОМ");
                    case (5):
                        System.out.print("УПЫРЬ");
                    case (6):
                        System.out.print("БОРОДАЧ");
                    case (7):
                        System.out.print("БОБЕР");
                    case (8):
                        System.out.print("КОРОЛЬ");
                    case (9):
                        System.out.print("ТОЛСТОПУЗ");
                    case (10):
                        System.out.print("КИЛЛЕР");
                }
        }

    }

    public static int getLenght(long numb) {
        int count1 = (numb == 0) ? 1 : 0;
        while (numb != 0) {
            count1++;
            numb /= 10;
        }
        System.out.println(count1);
        return count1;
    }
}
