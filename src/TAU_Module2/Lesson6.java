package TAU_Module2;

public class Lesson6 {
    public static void main(String[] args) {
        //1

        int[] array = {1, -2, 3, 4, 5, 6, 7, 8, 9};

        for (int i = array.length - 1; i >= 0; i--) {
            System.out.print(array[i]);
        }

        //2

        int[] a = {7, 3, 10, -5, 13, -7, 0, -1, 4};
        int sum = 0;
        for (int i = 1; i <= a.length; i++) {
            if ((i % 2 == 0) && (a[i] > 0)) {
                sum += a[i];
            }
        }
        System.out.println("\n" + "Sum: " + sum);
    }
}

