package TAU_Module2;

public class Lesson7 {
    public static void main(String[] args) {
        //Формула чила Фибоначчи
        //F1 = 1
        //F2 = 1
        //Fn = Fn-1 + Fn-2

        //С помощью цикла for
        int n0 = 1;
        int n1 = 1;
        int n2;
        System.out.print(n0 + " " + n1 + " ");
        for (int i = 3; i <= 11; i++) {
            n2 = n0 + n1;
            System.out.print(n2 + " ");
            n0 = n1;
            n1 = n2;
        }
        System.out.println();
    }

    //Рекурсивно
    public static int fibonachi(int n) {
        if (n == 0) {
            return 0;
        } else if (n == 1) {
            return 1;
        } else {
            return fibonachi(n - 1) + fibonachi(n - 2);
        }
    }
}
